$(window).load(function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $('body').addClass('ios');
    } else {
        $('body').addClass('web');
    };
    $(".img-js").each(function () {
        $(this).attr('src', $(this).data("src"));
    });
    $('.bg-js').each(function () {
        var thisPoster = $(this).attr('data-poster')
        $(this).css({
            'background-image': 'url(' + thisPoster + ')',

        })
    })
    $('body').removeClass('loaded');
});
/* viewport width */
function viewport() {
    var e = window,
        a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {
        width: e[a + 'Width'],
        height: e[a + 'Height']
    }
};
/* viewport width */
$(function () {
    /* placeholder*/
    $('input, textarea').each(function () {
        var placeholder = $(this).attr('placeholder');
        $(this).focus(function () {
            $(this).attr('placeholder', '');
        });
        $(this).focusout(function () {
            $(this).attr('placeholder', placeholder);
        });
    });
    /* placeholder*/

    $('.button-nav').click(function () {
        $(this).toggleClass('active'),
            $('.main-nav-list').slideToggle();
        return false;
    });

    /* components */

    /*
    
    if($('.styled').length) {
    	$('.styled').styler();
    };
    if($('.fancybox').length) {
    	$('.fancybox').fancybox({
    		margon  : 10,
    		padding  : 10
    	});
    };
    if($('.slick-slider').length) {
    	$('.slick-slider').slick({
    		dots: true,
    		infinite: false,
    		speed: 300,
    		slidesToShow: 4,
    		slidesToScroll: 4,
    		responsive: [
    			{
    			  breakpoint: 1024,
    			  settings: {
    				slidesToShow: 3,
    				slidesToScroll: 3,
    				infinite: true,
    				dots: true
    			  }
    			},
    			{
    			  breakpoint: 600,
    			  settings: "unslick"
    			}				
    		]
    	});
    };
    if($('.scroll').length) {
    	$(".scroll").mCustomScrollbar({
    		axis:"x",
    		theme:"dark-thin",
    		autoExpandScrollbar:true,
    		advanced:{autoExpandHorizontalScroll:true}
    	});
    };
    
    */

    /* components */

    $('[data-fancybox="doc"]').fancybox({

        baseClass: 'company-modal',
        touch: {
            vertical: false, // Allow to drag content vertically
            momentum: true // Continue movement after releasing mouse/touch when panning
        },

    });

});

$('.input-placeholder-js').live('change', function () {
    var thisVal = $(this)
    if (thisVal.val().length > 0) {
        thisVal.parents('.section-form__group').find('.form-placeholder').addClass('active')
    } else {
        thisVal.parents('.section-form__group').find('.form-placeholder').removeClass('active')
    }
})
//$('.input-placeholder-js').live('keyup', function () {
//    var thisVal = $(this)
//    if (thisVal.val().length > 0 ) {
//        thisVal.parents('.section-form__group').find('.form-placeholder').addClass('active')
//    } 
//})
var inpFocus
$('.input-placeholder-js').focusin(function () {
    inpFocus = $(this)
    inpFocus.parents('.section-form__group').find('.form-placeholder').addClass('focus')
})
$('.input-placeholder-js').focusout(function () {
    var thisVal = $(this)
    if (thisVal.val().length > 0) {
        inpFocus.parents('.section-form__group').find('.form-placeholder').addClass('focus')
    } else {
        inpFocus.parents('.section-form__group').find('.form-placeholder').removeClass('focus')
    }

})
$('.header-gamburger').click(function () {
    $(this).toggleClass('active')
    $('header').addClass('anim')
    $('.header-wrapper').toggleClass('active')
    $('body').toggleClass('active')
    return false
})

$('.btn-back-js').click(function () {
    $(this).parents('.section-form__box').find('.section-form__wrap').removeClass('hide')
    $(this).parents('.section-form__box').find('.section-form__complete').removeClass('active')
    return false
})

// $('.news-form__wrap .btn-submit-js').click(function () {
//     $(this).parents('.section-form__box').find('.section-form__block').addClass('hide');
//     $(this).addClass('hide');
//     $(this).parents('.section-form__box').find('.section-form__complete').addClass('active');
//     return false;
// });

var flag = 0;
$(window).scroll(function () {

    if ($(this).scrollTop() > 80) {

        $('header').addClass("sticky");




        flag = 1
    } else {
        $('header').removeClass("sticky");


    }

});
var thisHover
var thisTime
$('.section-services__list li').hover(function () {
    thisHover = $(this)
    thisTime = setTimeout(function () {
        thisHover.find('.trans-show').addClass('active')
    }, 0)
}, function () {
    thisHover.find('.trans-show').removeClass('active')
    clearTimeout(thisTime)
})
var mobileHover = function () {
    $('.company-employee__list > li').on('touchstart', function () {
        $(this).trigger('hover');
    }).on('touchend', function () {
        $(this).trigger('hover');
    });
};

mobileHover();
if ($('.btn-modal-js').length) {
    $('.btn-modal-js').fancybox({
        baseClass: 'packet-modal-wrap',


        infoBAr: false,
        arrows: false,
        hash: false,
        btnTpl: {
            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}">' +
                '<svg width="58" height="58" viewBox="0 0 58 58" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="43.1427" y1="14.858" x2="14.8584" y2="43.1422" stroke="black" stroke-width="2"/><line x1="43.1425" y1="43.1422" x2="14.8582" y2="14.858" stroke="black" stroke-width="2"/></svg>' +
                "</button>"
        },
        clickSlide: false,
        closeExisting: true,
        touch: false,
    });
}
if ($('.btn-pay-js').length) {


    $().fancybox({
        baseClass: 'packet-modal-wrap',
        selector: '.btn-pay-js',
        clickSlide: false,
        closeExisting: true,
        autoFocus: false,
        btnTpl: {
            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}">' +
                '<svg width="58" height="58" viewBox="0 0 58 58" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="43.1427" y1="14.858" x2="14.8584" y2="43.1422" stroke="black" stroke-width="2"/><line x1="43.1425" y1="43.1422" x2="14.8582" y2="14.858" stroke="black" stroke-width="2"/></svg>' +
                "</button>"
        },
        touch: {
            vertical: false, // Allow to drag content vertically
            horizontal: false,
            momentum: true // Continue movement after releasing mouse/touch when panning
        },
    });

}
if ($('.validate-form-js').length) {
    $(function () {
        $('.validate-form-js').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 1
                },
                lastName: {
                    required: true,
                    minlength: 1
                },
                middleName: {
                    required: true,
                    minlength: 1
                },
                email: {
                    required: true,
                    minlength: 1,

                },
                tel: {
                    required: true,
                    minlength: 1
                },
            },
            messages: {
                name: {
                    required: "Обязательное поле",

                },
                email: {
                    required: "Обязательное поле",
                    email: "Некорректно заполнен e-mail"
                },
                lastName: {
                    required: "Обязательное поле",

                },
                middleName: {
                    required: "Обязательное поле",

                },
                tel: {
                    required: "Обязательное поле",

                },

            },

        });
    });
}

if ($('.validate-form-next-js').length) {
    $('.validate-form-next-js').each(function () {
        $(this).validate({
            rules: {
                name: {
                    required: true,
                    minlength: 1
                },
                lastName: {
                    required: true,
                    minlength: 1
                },
                middleName: {
                    required: true,
                    minlength: 1
                },
                email: {
                    required: true,
                    minlength: 1,

                },
                tel: {
                    required: true,
                    minlength: 1
                },
            },
            messages: {
                name: {
                    required: "Обязательное поле",

                },
                email: {
                    required: "Обязательное поле",
                    email: "Некорректно заполнен e-mail"
                },
                lastName: {
                    required: "Обязательное поле",

                },
                middleName: {
                    required: "Обязательное поле",

                },
                tel: {
                    required: "Обязательное поле",

                },

            },

            submitHandler: function (element) {
                if ($(element).parents('.section-form__box').length == true) {
                    $(element).parents('.section-form__box').find('.section-form__wrap').addClass('hide')
                    $(element).parents('.section-form__box').find('.section-form__complete').addClass('active')

                }

            }
        });
    })
}

if ($('.js-news-form__wrap').length) {
    $('.js-news-form__wrap').each(function () {
        $(this).validate({
            rules: {
                email: {
                    required: true,
                    minlength: 1,

                }
            },
            messages: {
                email: {
                    required: "Обязательное поле",
                    email: "Некорректно заполнен e-mail"
                }
            },

            submitHandler: function (element) {
                if ($(element).parents('.section-form__box').length == true) {
                    $(element).parents('.section-form__box').find('.section-form__block').addClass('hide');
                    $(element).parents('.section-form__box').find('.btn-submit-js').addClass('hide');
                    $(element).parents('.section-form__box').find('.section-form__complete').addClass('active');
                }
            }
        });
    })
}
$('.question-block-js').click(function(){
    $(this).find('.questions-tabs__ask').slideToggle('fast' , function(){})
    $(this).siblings('.question-block-js').find('.questions-tabs__ask').slideUp('fast' , function(){})
    $(this).siblings('.question-block-js').removeClass('show')
    $(this).toggleClass('show')
})

if ($('input[name="tel"]').length) {
    $('input[name="tel"]').inputmask({
        mask: '+7 (999) 999-99-99',
        showMaskOnHover: false,
    });
}
$('.questions-list__nav li a').click(function(){
    var thisAttr = $(this).attr('href')
    $(this).addClass('active')
    $(this).parent('li').siblings('li').find('a').removeClass('active')
    $(thisAttr).siblings('.questions-tabs__content').removeClass('active')
    $(thisAttr).addClass('active')
    return false
})
if ($('.swiper-container').length) {
    var swiper = new Swiper('.swiper-container', {
        pagination: {
            el: '.swiper-pagination',
            type: 'progressbar',
        },
        spaceBetween: 20,
        on: {
            slideChange: function (currentClass, totalClass) {

                var actIn = this.activeIndex + 1;
                var totalLength = this.slides.length
                if (actIn >= 10) {
                    $('.swiper-active-index').text(actIn)
                }
                else {
                    $('.swiper-active-index').text('0' + actIn)
                }
                $('.swiper-total').text(this.slides.length)

                if (totalLength >= 10) {
                    $('.swiper-total').text(totalLength)
                }
                else {
                    $('.swiper-total').text('0' + totalLength)
                }
            },
            init: function (currentClass, totalClass) {

                var actIn = this.activeIndex + 1;
                var totalLength = this.slides.length
                if (actIn >= 10) {
                    $('.swiper-active-index').text(actIn)
                }
                else {
                    $('.swiper-active-index').text('0' + actIn)
                }
                $('.swiper-total').text(this.slides.length)

                if (totalLength >= 10) {
                    $('.swiper-total').text(totalLength)
                }
                else {
                    $('.swiper-total').text('0' + totalLength)
                }
            },
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

}
if ($('.custom-scroll-js').length) {
    $(".custom-scroll-js").mCustomScrollbar({
        axis: "y",
        theme: "dark-thin",
        autoExpandScrollbar: true,
        advanced: { autoExpandHorizontalScroll: true }
    });
};
var handler = function () {

    var height_footer = $('footer').height();
    var height_header = $('header').height();
    //$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});


    var viewport_wid = viewport().width;
    var viewport_height = viewport().height;

    if (viewport_wid <= 991) {

    }

}
$(window).bind('load', handler);
$(window).bind('resize', handler);
